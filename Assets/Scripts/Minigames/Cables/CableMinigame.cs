﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using System.Linq;
namespace Minigames.Cable
{
	public class CableMinigame : Minigame
	{
		public AudioSource cableConnectionSound;
		[SerializeField]
		private Cable[] cables, targets;
		[SerializeField]
		private Color[] cableColors;
		private Coroutine completionCheck;
		private void Awake()
		{
			Assert.AreEqual(cables.Length, targets.Length);
			Assert.IsTrue(cableColors.Length >= cables.Length);
		}
		public override void Restart()
		{
			if (completionCheck != null) StopCoroutine(completionCheck);
			completionCheck = StartCoroutine(CompletionCheckCoroutine());

			Color[] shuffledColors = Shuffle(cableColors);
			
			int[] targetIndexes = new int[targets.Length];
			for (int i = 0; i < targets.Length; i++)
			{
				targetIndexes[i] = i; // Init the array to {0, 1, 2, 3...}
				
			}
			targetIndexes = Shuffle(targetIndexes);

			for (int i = 0; i < cables.Length; i++)
			{
				cables[i].Color = shuffledColors[i];// Set the color of the cable.
				targets[targetIndexes[i]].Color = shuffledColors[i];// Set the target's color.
				targets[i].EnableWire(true);
				targets[i].ResetCable();
				cables[i].EnableWire(true);
				cables[i].ResetCable();
			}
		}
		private IEnumerator CompletionCheckCoroutine(float interval = 1f)
		{
			yield return new WaitForSecondsRealtime(interval);
			while (!IsComplete())
			{
				yield return new WaitForSecondsRealtime(interval);
			}
			yield return new WaitForSecondsRealtime(interval);
			Complete();
		}
		public override void Complete()
		{
			cableConnectionSound.Play();
			base.Complete();
		}
		private bool IsComplete() => cables.All(c => c.connected);
		private static T[] Shuffle<T>(T[] colors)
		{
			var r = new System.Random();
			var shuffledItems = colors.OrderBy(c=>r.Next());
			return shuffledItems.ToArray();
		}
	}
}