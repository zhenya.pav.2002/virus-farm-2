﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace Minigames.Cable
{
	[RequireComponent(typeof(Image))]
    public class Cable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
		public Color Color
		{
			get => color; set
			{
				color = value;
				if (image) image.color = value;
			}
		}
		public bool connected = false;

		[SerializeField]
		private GameObject cableExtensionPrefab;
		[SerializeField]
		private Vector2 cableExtensionOffset;
		[SerializeField]
		private bool hideWireOnConnection = true;

		private bool isDragActive = false;
		private RectTransform cableExtension, canvasTransform;
		private GraphicRaycaster raycaster;
		private Color color = Color.white;
		private Image image;
		private GameObject wire;
		private void Awake()
		{
			image = GetComponent<Image>();
			canvasTransform = GetComponentInParent<Canvas>().GetComponent<RectTransform>();
			raycaster = canvasTransform.GetComponent<GraphicRaycaster>();
			wire = transform.GetChild(0).gameObject;
		}

		public void OnDrag(PointerEventData eventData)
		{
			if (!connected && isDragActive)
			{
				Vector3 pointerPosition = Camera.main.ScreenToWorldPoint(eventData.position);
				Vector3 startPosition = transform.TransformPoint(cableExtensionOffset);

				cableExtension.StretchBetween(startPosition, pointerPosition);

				//Check if pointer is over a cable
				var raycastResult = new List<RaycastResult>();
				raycaster.Raycast(eventData, raycastResult);
				raycastResult.ForEach(rr =>
				{
					if(rr.gameObject.TryGetComponent(out Cable cable) &&
						cable != this)
					{
						if (cable.Color == this.Color)
						{
							ConnectCable(cable);
						}
						else
						{
							GameManager.instance.errorSound.Play();
							OnEndDrag(eventData);
						}
					}
				});
			}
		}
		public void ResetCable()
		{
			if (cableExtension) Destroy(cableExtension.gameObject);
			connected = false;
		}
		private void ConnectCable(Cable target)
		{
			Vector3 startPosition = transform.TransformPoint(cableExtensionOffset);
			Vector3 endPosition = target.transform.TransformPoint(target.cableExtensionOffset);

			cableExtension.StretchBetween(startPosition, endPosition);

			connected = true;
			target.connected = true;

			if(hideWireOnConnection) EnableWire(false);
			if (target.hideWireOnConnection) target.EnableWire(false);

		}
		public void EnableWire(bool enable)
		{
			if (wire) wire.SetActive(enable);
		}
		public void OnBeginDrag(PointerEventData eventData)
		{
			isDragActive = true;
			cableExtension = Instantiate(cableExtensionPrefab, transform).GetComponent<RectTransform>();
			cableExtension.GetComponent<Image>().color = Color;
			OnDrag(eventData);
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			isDragActive = false;
			if (!connected && cableExtension) Destroy(cableExtension.gameObject);
		}
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.black;
			Gizmos.DrawWireSphere(transform.TransformPoint(cableExtensionOffset), .5f);
		}
	}
}