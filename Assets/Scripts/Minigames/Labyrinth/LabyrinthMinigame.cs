﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Minigames.Labyrinth
{
	public class LabyrinthMinigame : Minigame
	{
		public GameObject helpText;

		public GameObject pointerPrefab;
		public GameObject tailPrefab;

		public GraphicRaycaster raycaster;

		public AudioSource blip;

		[SerializeField]
		private Level[] levels;

		private Level currentLevel;
		private void Awake()
		{
			if (!raycaster) raycaster = GetComponentInParent<GraphicRaycaster>();
		}
		public override void Restart()
		{
			foreach (var l in levels)
			{
				l.gameObject.SetActive(false);
			}
			currentLevel = levels.GetRandomItem();
			currentLevel.gameObject.SetActive(true);
		}
	}
}