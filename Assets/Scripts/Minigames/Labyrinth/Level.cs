﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;
namespace Minigames.Labyrinth
{
    public class Level : MonoBehaviour
    {
        const string obstacleTag = "Obstacle",
                     finishTag = "Finish";
        const float movementSpeed = 50f;

        [SerializeField] private Vector2 startPosition;
        [SerializeField] private Vector2 startDirection;

        private Vector2 direction, lastDirection, currentPosition, lastTurnPosition;
        private bool started = false;
        private GameObject pointer;
        private LabyrinthMinigame minigame;
        private List<RectTransform> tails = new List<RectTransform>();
		private void Awake()
		{
            minigame = GetComponentInParent<LabyrinthMinigame>();
		}
		private void FixedUpdate()
		{
            Vector2 input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
			if (input != Vector2.zero)
			{
                if (Mathf.Abs(input.x) > Mathf.Abs(input.y))
                    input.y = 0;
                else
                    input.x = 0;
                direction = Vector2.ClampMagnitude(input,1f);
				if (direction == -lastDirection)
				{
                    direction = lastDirection;
				}
                if (!started)
                {
                    currentPosition = startPosition;
                    lastTurnPosition = currentPosition;
                    direction = startDirection;
                    pointer = Instantiate(minigame.pointerPrefab, transform);
                    pointer.transform.position = transform.TransformPoint(currentPosition);
                    minigame.helpText.SetActive(false);
                    started = true;
                }
            }
            if (started)
            {
                currentPosition += movementSpeed * Time.fixedDeltaTime * direction.normalized;
                pointer.transform.position = transform.TransformPoint(currentPosition);

                if (lastDirection != direction || !tails.Any())
                {
                    minigame.blip.Play();
                    if (tails.Count > 1) tails.Last().tag = obstacleTag;
                    tails.Add(Instantiate(minigame.tailPrefab, transform).GetComponent<RectTransform>());
                    lastTurnPosition = currentPosition;
                    lastDirection = direction;
                }
                SetTail(tails.Last(), lastTurnPosition - direction * 1.5f, currentPosition - direction / 2);
                lastDirection = direction;

                CheckCollision();
            }
        }
		public void ResetLevel()
		{
            started = false;
            minigame.helpText.SetActive(true);
            Destroy(pointer);
            tails.ForEach(t => Destroy(t.gameObject));
            tails = new List<RectTransform>();
		}
        private void SetTail(RectTransform tail, Vector2 startPos, Vector2 endPos)
		{
            tail.StretchBetween(transform.TransformPoint(startPos), transform.TransformPoint(endPos));
		}
		private void CheckCollision()
        {
            var eventData = new PointerEventData(EventSystem.current);
            eventData.position = RectTransformUtility.WorldToScreenPoint(Camera.main, pointer.transform.position);
            var raycastResults = new List<RaycastResult>();
            minigame.raycaster.Raycast(eventData, raycastResults);
            raycastResults.ForEach(r =>
            {
                switch (r.gameObject.tag)
                {
                    case obstacleTag:
                        GameManager.instance.errorSound.Play();
                        ResetLevel();
                        return;
                    case finishTag:
                        minigame.Complete();
                        ResetLevel();
                        return;
                }
            });
        }

		private void OnDrawGizmosSelected()
		{
            Gizmos.color = Color.green;
            Vector3 pos = transform.TransformPoint(startPosition);
            Gizmos.DrawWireSphere(pos, .5f);
            Gizmos.DrawRay(pos, startDirection);
            Gizmos.color = Color.cyan;
            pos = transform.TransformPoint(currentPosition);
            Gizmos.DrawWireSphere(pos, .5f);
            Gizmos.DrawRay(pos, direction);
        }
    }
}