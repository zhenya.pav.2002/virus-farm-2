﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Minigames.Injection
{
	public class InjectionMinigame : Minigame
	{
		public Rect obstacleBounds;
		public float obstacleSpeed;
		[SerializeField] private List<Obstacle> obstacles;

		[Header("Visual")]
		[SerializeField] private Color defaultObstacleColor = Color.white;
		[SerializeField] private Color currentObstacleColor = Color.yellow;
		[SerializeField] private Color failObstacleColor = Color.red;
		[SerializeField] private Color successObstacleColor = Color.green;

		private IEnumerator<Obstacle> enumerator;
		
		public override void Restart()
		{
			foreach (var o in obstacles)
			{
				o.RestartAnimation();
				o.SetColor(defaultObstacleColor);
			}
			enumerator = obstacles.GetEnumerator();
			MoveToNextObstacle();
		}
		private void MoveToNextObstacle()
		{
			if (!enumerator.MoveNext())
			{
				Invoke(nameof(Complete), 1f);
			}
			else
			{
				enumerator.Current.SetColor(currentObstacleColor);
			}
			
		}
		private void Update()
		{
			if (Input.GetButtonDown("Submit") && enumerator.Current.enabled)
			{
				if (enumerator.Current.IsWithinSafeInterval())
				{
					// Success.
					enumerator.Current.SetColor(successObstacleColor);
					enumerator.Current.enabled = false;
					MoveToNextObstacle();
				}
				else
				{
					// Fail.
					GameManager.instance.errorSound.Play();
					enumerator.Current.SetColor(failObstacleColor);
					enumerator.Current.enabled = false;
					Invoke(nameof(Restart), 1f);
				}
			}
		}
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.cyan;
			Vector3 position = transform.TransformPoint(obstacleBounds.position);
			Vector3 size = transform.TransformVector(obstacleBounds.size);
			size.z = 1f;
			Gizmos.DrawWireCube(position+size/2, size);
		}
	}
}