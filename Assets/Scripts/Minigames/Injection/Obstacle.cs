﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Minigames.Injection
{
	public class Obstacle : MonoBehaviour
	{
		[SerializeField] private float safeInterval;


		private Image[] images;
		private InjectionMinigame minigame;
		private float velocity;
		private Vector3 startingPosition;
		private void Awake()
		{
			minigame = GetComponentInParent<InjectionMinigame>();
			images = GetComponentsInChildren<Image>();
			startingPosition = transform.localPosition;
		}
		public void RestartAnimation()
		{
			startingPosition.y = Random.Range(-safeInterval, safeInterval);
			transform.localPosition = startingPosition;
			enabled = true;
			velocity = Random.Range(-1f, 1f);
			if (Mathf.Abs(velocity) < .25f) velocity /= Mathf.Abs(velocity);
			velocity *= minigame.obstacleSpeed;
		}
		public void SetColor(Color color)
		{
			foreach (var image in images)
			{
				image.color = color;
			}
		}
		public bool IsWithinSafeInterval() => Mathf.Abs(transform.localPosition.y) <= safeInterval / 2f;
		private void FixedUpdate()
		{
			transform.localPosition += new Vector3(0,velocity * Time.fixedDeltaTime);
			if (!minigame.obstacleBounds.Contains(transform.localPosition,true))
			{
				Debug.Log(transform.localPosition, this);
				velocity = -velocity;
			}
		}
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.green;
			Gizmos.DrawCube(transform.position, transform.TransformVector(new Vector3(2, safeInterval)));
		}
	}
}