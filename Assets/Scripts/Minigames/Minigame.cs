﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Minigames
{
	public abstract class Minigame : MonoBehaviour
	{
		public event System.Action OnComplete;
		public virtual void Begin()
		{
			gameObject.SetActive(true);
			Restart();
		}
		public abstract void Restart();
		public virtual void Complete()
		{
			Close();
			OnComplete?.Invoke();
		}
		public virtual void Close()
		{
			gameObject.SetActive(false);
			//Restart();
		}
	}
}