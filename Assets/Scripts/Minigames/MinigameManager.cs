﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Minigames
{
    public class MinigameManager : MonoBehaviour
    {
        public const string shownIntroKey = "Minigames_Intro_Shown";
        public static MinigameManager Instance { get; private set; }

        public event System.Action OnAllMinigamesComplete, OnMinigamesStarted;

        public bool minigameActive = false;
        public List<Minigame> minigames;
        List<Minigame>.Enumerator enumerator;

		private void Awake()
        {
            Instance = this;
        }
        public void StartMinigames()
        {
            if (minigameActive) return;

			if (PlayerPrefs.GetInt(shownIntroKey, 0) == 1)
			{
                enumerator = minigames.GetEnumerator();
                OnMinigamesStarted?.Invoke();
                PlayNextMinigame();
            }
			else
			{
                StartCoroutine(ShowIntro());
			}
        }
        private IEnumerator ShowIntro()
		{
            PauseMenu.instance.SetIntroActive(true);

            yield return new WaitWhile(() => PauseMenu.instance.introMenu.activeSelf);

            PlayerPrefs.SetInt(shownIntroKey, 1);
            StartMinigames();
        }
        public void Close()
		{
			foreach (var game in minigames)
			{
                game.Close();
			}
            minigameActive = false;

        }
        private void PlayNextMinigame()
        {
            if (enumerator.MoveNext())
            {
                enumerator.Current.OnComplete += HandleMinigameCompletion;
                enumerator.Current.Begin();
                minigameActive = true;
            }
            else
            {
                minigameActive = false;
                OnAllMinigamesComplete?.Invoke();
            }
        }
        private void HandleMinigameCompletion()
        {
            enumerator.Current.OnComplete -= HandleMinigameCompletion;
            PlayNextMinigame();
        }
    }
}