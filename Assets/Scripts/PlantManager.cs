using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#region Utility classes
[System.Serializable]
public class SerializableArray<T>
{
    public T[] plants = new T[4];
    public T this[int i]
    {
        get { return plants[i]; }
        set { plants[i] = value; }
    }
    public int Length => plants.Length;
}
[System.Serializable]
public class PlantArray : SerializableArray<Plant> { };
[System.Serializable]
public class GOArray : SerializableArray<GameObject> { };
#endregion

public class PlantManager : MonoBehaviour
{
    public static PlantManager staticRef;

    [Header("DNA Generation")]
    public PlantSprite sprout;
    [SerializeField] PlantSprite[] sprites;
    [SerializeField] Color[] plantColors, fruitColors;

    [Space]
    [SerializeField] private GameObject plantPrefab;

    private PlantDNA dna;

    private PlantArray[] plants = new PlantArray[3];
    private GOArray[] plantHolders = new GOArray[3];
    private System.Action[] dnaCreationDelegates;
    public void Awake()
    {
        staticRef = this;

        dnaCreationDelegates = new System.Action[]{
            SetRandomSprite,
            SetRandomPlantColor,
            SetRandomFruitColor
        };

        plants = new PlantArray[3];
        for (int i = 0; i < 3; i++)
        {
            plants[i] = new PlantArray();
        }
        

        plantHolders = new GOArray[3];
        for (int i = 0; i < 3; i++)
        {
            plantHolders[i] = new GOArray();
        }
        for (int i = 0; i < 12; i++)
        {
            plantHolders[i / 4][i % 4] = transform.GetChild(i).gameObject;
        }
    }
	private void Start()
	{
        if (!Minigames.MinigameManager.Instance) return;
        for (int i = 0; i < dnaCreationDelegates.Length; i++)
		{
            Minigames.MinigameManager.Instance.minigames[i].OnComplete += dnaCreationDelegates[i];
        }
		Minigames.MinigameManager.Instance.OnAllMinigamesComplete += OnAllMinigamesComplete;
		Minigames.MinigameManager.Instance.OnMinigamesStarted += OnMinigamesStarted;
	}
	private void OnDestroy()
	{
        if (!Minigames.MinigameManager.Instance) return;
        for (int i = 0; i < dnaCreationDelegates.Length; i++)
        {
            Minigames.MinigameManager.Instance.minigames[i].OnComplete -= dnaCreationDelegates[i];
        }
        Minigames.MinigameManager.Instance.OnAllMinigamesComplete -= OnAllMinigamesComplete;
        Minigames.MinigameManager.Instance.OnMinigamesStarted -= OnMinigamesStarted;
    }
    private void OnAllMinigamesComplete()
    {
        int x, y;
        do // Get random unoccupied plant holder coords
        {
            x = Random.Range(0, plantHolders.Length);
            y = Random.Range(0, plantHolders[x].Length);
        } while (plants[x][y]);

        AddPlant(dna, x, y);
    }
    private void OnMinigamesStarted()
    {
        dna = ScriptableObject.CreateInstance<PlantDNA>();    
    }
    private void SetRandomFruitColor()
    {
        dna.fruitColor = fruitColors.GetRandomItem();
        Debug.Log(dna.fruitColor, this);
    }
    private void SetRandomPlantColor()
    {
        dna.color = plantColors.GetRandomItem();
        Debug.Log(dna.color, this);
    }
    private void SetRandomSprite()
	{
        dna.form = sprites.GetRandomItem();
        Debug.Log(dna.form, this);
    }

    public void AddPlant(PlantDNA dna, int x, int y)
	{
        plants[x][y] = Instantiate(plantPrefab, plantHolders[x][y].transform).GetComponent<Plant>();
        plants[x][y].dna = dna;
    }

	#region Saves
	[System.Serializable]
    class PlantManagerSaveData
    {
        public string[] strings = new string[12];
    }
    PlantManagerSaveData saveData = new PlantManagerSaveData();
    public string Serialize()
    {
        for (int i = 0; i < 12; i++)
        {
            saveData.strings[i] = (plants[i / 4][i % 4]) ? plants[i / 4][i % 4].Serialize():"";
        }
        return JsonUtility.ToJson(saveData);
    }

    public void Deserialize(string data)
    {
        saveData = JsonUtility.FromJson<PlantManagerSaveData>(data);
        for (int i = 0; i < 12; i++)
        {
            if(plantHolders[i / 4][i % 4].transform.childCount>0)
                Destroy(plantHolders[i / 4][i % 4].transform.GetChild(0).gameObject);

            if (saveData.strings[i] != "")
            {
                var n = Instantiate(plantPrefab, plantHolders[i / 4][i % 4].transform).GetComponent<Plant>();
                n.Deserialize(saveData.strings[i]);
            }
        }
    }
	#endregion
}