using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "DNA", menuName = "GameData/DNA")]
public class PlantDNA : ScriptableObject
{
    public float fruitPrice = 100f;
    public const float adultAge = 12f;
    public const float fruitAge = 20f;

    [Header("Visual")]
    public PlantSprite form;
    /// <summary>
    /// Color of sprite
    /// </summary>
    public Color color = Color.green;
    public Color fruitColor = Color.red;
	#region Deprecated
	/*[Header("Reproduction")]
    /// <summary>
    /// Scale of parameter change on mutation
    /// </summary>
    public float mutationRate = .1f;
    /// <summary>
    /// Reproduction mean time to happen, seconds
    /// Counts only when plant is adult;
    /// </summary>
    public float reproductionMTTH = 30;
    /// <summary>
    /// Low stability increases chance of disaser
    /// </summary>
    public float stability = 1;
    /// <summary>
    /// Death mean time to happen, seconds
    /// </summary>
    public float deathMTTH = 50;*/

	/*/// <summary>
    /// Age of adulthood, seconds
    /// </summary>
    public float adultAge = 30;
    public float fruitMTTH = 10;

    public float fruitPrice { get { return (1 + 1 / stability + fruitMTTH); } }
    public PlantDNA Mutation(PlantSprite _form, float _fertModifier, float _lifeTimeModifier, Color _colorModifier, Color _fruitColorModifier, float _stabilityModifier)
    {
        PlantDNA dna = Instantiate(this);
        Debug.Log(JsonUtility.ToJson(dna));

        dna.stability *= 1 + _stabilityModifier * mutationRate;
        dna.mutationRate *= 1 + (_stabilityModifier * mutationRate);

        dna.reproductionMTTH *= 1 + (_fertModifier * mutationRate);
        dna.fruitMTTH *= 1 + (_fertModifier * mutationRate);

        dna.reproductionMTTH *= 1 + (_lifeTimeModifier * mutationRate);
        dna.deathMTTH *= 1 + (_lifeTimeModifier * mutationRate);

        dna.color = Color.Lerp(dna.color, _colorModifier, .5f + mutationRate);
        dna.form = _form;

        dna.fruitColor = Color.Lerp(dna.fruitColor, _fruitColorModifier, .5f + mutationRate);

        Debug.Log(JsonUtility.ToJson(dna));
        return dna;
    }*/
	#endregion
}