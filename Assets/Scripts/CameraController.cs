using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class CameraController : MonoBehaviour
{
    public float leftLimit, rightLimit;
    public Transform follow;
    public new Camera camera;
	private void Awake()
	{
        if (!follow) enabled = false;
	}
	// Update is called once per frame
	void LateUpdate()
    {
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(follow.position.x, leftLimit + camera.orthographicSize * 1.8f, rightLimit - camera.orthographicSize * 1.8f);
        transform.position = pos;
#if UNITY_ANDROID
        Touch touch = Input.GetTouch(0);
        if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
        {
            if (touch.position.x > Screen.currentResolution.width * 2 / 3) PlayerController.staticRef.xInput = 1; else
                if(touch.position.x < Screen.currentResolution.width/ 3) PlayerController.staticRef.xInput = -1;
        }
#endif
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(new Vector3((leftLimit + rightLimit) / 2, 0, 0), new Vector3((leftLimit - rightLimit), 10, 0));
    }
}
