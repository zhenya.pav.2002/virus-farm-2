using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    
    [AddComponentMenu("Event/Graphic Raycaster 2")]
    [RequireComponent(typeof(Canvas))]
    public class GraphicRaycaster2 : GraphicRaycaster
    {
        float xMod, yMod;
        protected override void Awake()
        {
            base.Awake();
            Resolution r = Screen.currentResolution;
            xMod = r.width / 480;
            yMod = r.height / 270;
        }
        public override void Raycast(PointerEventData eventData, List<RaycastResult> resultAppendList)
        {
            Vector2 v = eventData.position;
            v.x /= xMod;
            v.y /= yMod;
            eventData.position = v;
            base.Raycast(eventData, resultAppendList);
            
        }
    }
}
