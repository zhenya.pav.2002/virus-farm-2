﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class NewGameButton : StaticMethodButton
{
	protected override List<UnityAction> Actions => new List<UnityAction>()
	{
		GameManager.NewGame,
		DisableButton
	};
}
