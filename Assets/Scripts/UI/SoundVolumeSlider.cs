﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
public class SoundVolumeSlider : MonoBehaviour
{
	[SerializeField] private bool isPersistent = true;
	[SerializeField] AudioMixer mixer;
	[SerializeField] private string parameter;
	private void Awake()
	{
		if (isPersistent)
		{
			float volume = PlayerPrefs.GetFloat(parameter, 0f);
			GetComponent<UnityEngine.UI.Slider>().value = volume;
			UpdateVolume(volume);
		}
	}
	public void UpdateVolume(float volume)
	{
		mixer.SetFloat(parameter, volume);
		if(isPersistent)PlayerPrefs.SetFloat(parameter, volume);
	}
}
