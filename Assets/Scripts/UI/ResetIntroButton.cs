﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ResetIntroButton : StaticMethodButton
{
	private void OnEnable()
	{
		button.interactable = PlayerPrefs.GetInt(GameManager.shownIntroSceneKey, 0) == 1;
	}
	protected override List<UnityAction> Actions => new List<UnityAction>()
	{
		GameManager.ResetIntro,
		OnEnable
	};
}
