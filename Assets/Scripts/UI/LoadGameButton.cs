﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadGameButton : StaticMethodButton
{
	protected override List<UnityAction> Actions => new List<UnityAction>()
	{
		GameManager.LoadGame,
		DisableButton
	};
}
