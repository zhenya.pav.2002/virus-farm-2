﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public abstract class StaticMethodButton : MonoBehaviour
{
	protected Button button;
	protected abstract List<UnityEngine.Events.UnityAction> Actions { get; }
	private void Awake()
	{
		button = GetComponent<Button>();
		foreach (var action in Actions)
		{
			button.onClick.AddListener(action);
		}
	}
	private void OnDestroy()
	{
		foreach (var action in Actions)
		{
			button.onClick.RemoveListener(action);
		}
	}
	protected void DisableButton()
	{
		button.interactable = false;
	}
}
