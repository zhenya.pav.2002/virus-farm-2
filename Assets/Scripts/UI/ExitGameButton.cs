﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ExitGameButton : StaticMethodButton
{
	protected override List<UnityAction> Actions => new List<UnityAction>()
	{
		GameManager.ExitGame
	};
}
