﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static PauseMenu instance;

    public GameObject escMenu, settingsMenu, introMenu, aboutMenu;

    public AudioSource buttonSound;

    [Space]
    [SerializeField]
    private GameObject moneyIndicator;

    private void Awake()
	{
        instance = this;
        SetEscMenuActive(false);
        SetSettingsMenuActive(false);
	}
    public static void SetPause(bool paused)
	{
        Time.timeScale = paused ? 0f : 1f;
        if (instance && instance.moneyIndicator) 
            instance.moneyIndicator.SetActive(!paused);
    }
    public void SetSettingsMenuActive(bool active)
	{
        SetPause(active);
        settingsMenu.SetActive(active);
        if(active) buttonSound.Play();
    }
    public void SetEscMenuActive(bool active)
	{
        if (active)
        {
            Minigames.MinigameManager.Instance.Close();
            if (settingsMenu.activeSelf) SetSettingsMenuActive(false);
        }
        else
        {
            if (PlayerController.staticRef)
                PlayerController.staticRef.enabled = true;
        }
        SetPause(active);
        escMenu.SetActive(active);
        if (active) buttonSound.Play();
    }
    public void SetIntroActive(bool active)
    {
        SetPause(active);
        introMenu.SetActive(active);
    }
	private void LateUpdate()
	{
        if (Input.GetButtonDown("EscMenu"))
        {
            SetEscMenuActive(!escMenu.activeSelf);
        }
    }
}
