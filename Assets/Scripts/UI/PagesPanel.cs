﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PagesPanel : MonoBehaviour
{
	private const string inputAxisName = "Horizontal";

    [SerializeField]
    private GameObject[] pages;
	[SerializeField]
	private Button nextButton, prevButton, closeButton;
	[SerializeField]
	private bool closeOnlyOnLastPage, keyboardInput;

    private int currentPage = 0;
	private float lastAxisInput = 0;
	private void Start()
	{
		Refresh();
		lastAxisInput = Input.GetAxisRaw(inputAxisName);
	}
	private void Update()
	{
		if (Input.GetAxisRaw(inputAxisName) > lastAxisInput && lastAxisInput == 0) NextPage();
		else if (Input.GetAxisRaw(inputAxisName) < lastAxisInput && lastAxisInput == 0) PrevPage();
		lastAxisInput = Input.GetAxisRaw(inputAxisName);
	}
	public void PrevPage()
	{
		currentPage--;
		Refresh();
	}
	public void NextPage()
	{
		currentPage++;
		Refresh();
	}
    private void Refresh()
	{
		for (int i = 0; i < pages.Length; i++)
		{
			pages[i].SetActive(i == currentPage);
		}
		prevButton.interactable = currentPage > 0;
		nextButton.interactable = currentPage < pages.Length - 1;

		closeButton.gameObject.SetActive(!closeOnlyOnLastPage || currentPage == pages.Length - 1);
	}
}
