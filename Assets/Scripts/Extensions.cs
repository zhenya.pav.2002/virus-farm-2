﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class Extensions
{
	/// <summary>
	/// 
	/// </summary>
	/// <param name="startPosition">Start point in the world space</param>
	/// <param name="endPosition">End point in the world space</param>
	public static void StretchBetween(this RectTransform transform, Vector3 startPosition, Vector3 endPosition)
	{
		endPosition.z = 0;
		startPosition.z = 0;

		float angle = Vector3.SignedAngle(Vector3.up, endPosition - startPosition, Vector3.forward);

		transform.localScale = transform.parent.localScale;
		transform.SetPositionAndRotation(
			startPosition,
			Quaternion.Euler(0, 0, angle + 90f));

		transform.sizeDelta = new Vector2(
			transform.InverseTransformVector(startPosition - endPosition).magnitude,
			transform.sizeDelta.y);
	}
	public static T GetRandomItem<T>(this IList<T> enumerable) => enumerable[Random.Range(0, enumerable.Count)];
	public static T GetRandomItem<T>(this T[] enumerable) => enumerable[Random.Range(0, enumerable.Length)];

	public static IEnumerator FadeTo(this Graphic graphic, Color targetColor, float duration)
	{
		float timeElapsed = 0;
		Color startingColor = graphic.color;
		while (timeElapsed <= duration)
		{
			timeElapsed += Time.unscaledDeltaTime;
			graphic.color = Color.Lerp(startingColor, targetColor, timeElapsed / duration);
			yield return new WaitForEndOfFrame();
		}
		graphic.color = targetColor;
	}
}
