using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public const string GameSceneName = "Game",
                 MainMenuSceneName = "MainMenu",
                 IntroSceneName = "Intro";

    public const string shownIntroSceneKey = "NewGame_Intro_Shown";

    public static GameManager instance;
    
    [Header("Audio clips")]
    public AudioSource moneySound;
    public AudioSource airSound, errorSound;

    public float money = 0;
    public Text moneyText;
    [System.Serializable]
    public class GameSave
    {
        public string plantSaveData;
        public float money = 0;
    }
    GameSave saveData = new GameSave();
    private void Awake()
    {
        instance = this;
    }

    public static void ResetIntro()
	{
        PauseMenu.instance.buttonSound.Play();
        PlayerPrefs.SetInt(shownIntroSceneKey, 0);
        PlayerPrefs.SetInt(Minigames.MinigameManager.shownIntroKey, 0);
	}

    public static void NewGame()
    {
        PauseMenu.instance.buttonSound.Play();
        if (PlayerPrefs.GetInt(shownIntroSceneKey, 0) == 1)
		{
            SceneManager.LoadSceneAsync(GameSceneName);
        }
		else
		{
            SceneManager.LoadSceneAsync(IntroSceneName);
        }
    }
    public static void LoadGame()
	{
        Debug.Log("LoadGame()");
        SceneManager.LoadSceneAsync(GameSceneName);
		SceneManager.sceneLoaded += SceneManager_sceneLoaded;
        PauseMenu.instance.buttonSound.Play();
    }
	private static void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
	{
        if (scene.name != GameSceneName) return;
        instance.LoadSave();
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

	public static void ExitGame()
    {
        PauseMenu.instance.buttonSound.Play();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public static IEnumerator MoneyAnimation()
    {
        instance.moneyText.color = Color.red;
        //play sound
        yield return new WaitForSecondsRealtime(1);
        instance.moneyText.color = Color.yellow;
    }



    public void SaveGame()
    {
        saveData.plantSaveData = PlantManager.staticRef.Serialize();
        PauseMenu.instance.buttonSound.Play();
        saveData.money = money;
        string save = JsonUtility.ToJson(saveData);
        Debug.Log(save);
        PlayerPrefs.SetString("save", save);
    }
    private void LoadSave()
    {
        string save = PlayerPrefs.GetString("save");
        Debug.Log(save);
        saveData = JsonUtility.FromJson<GameSave>(save);
        if(saveData == null)
		{
            Debug.Log("No save data found", this);
            return;
        }
        money = saveData.money;
        PlantManager.staticRef.Deserialize(saveData.plantSaveData);
    }
    private void LateUpdate()
    {
        moneyText.text = money.ToString();
    }
}