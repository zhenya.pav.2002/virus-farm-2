using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;
public class MutationController : MonoBehaviour
{
    /* Fert. Mod.
     * lifeTimeMod.
     * plantRMod.
     * plantGMod
     * plantBMod
     * fruitR
     * fruitG
     * fruitB
     * stability
     */
    public static MutationController staticRef;
    public float fert, lifeT, pR, pG, pB, fR, fG, fB, stab;
    public PlantDNA current, def;
    [System.NonSerialized]
    public GameObject[] sliders;
    public string[] texts;
    public Text descPanel;
    public string defaultDesc, lockedDesc;
    public bool[] knownParameters;
    public GameObject updateMenu;

    #region Slider Description
    PointerEventData eventData;
    EventSystem eventSystem;
    #endregion
    // Start is called before the first frame update
    void Awake()
    {
        staticRef = this;
        eventSystem = GetComponentInParent<EventSystem>();
        sliders = new GameObject[9];
        knownParameters = new bool[9];
        for (int i = 0; i < 9; i++)
        {
            sliders[i] = transform.GetChild(i).gameObject;
        }
        CloseTab();
        CloseUpdateMenu();
    }
    private void Update()
    {
        if (Input.GetButtonDown("Interact")) CloseTab();


        bool onSlider = false;
        eventData = new PointerEventData(eventSystem);
        eventData.position = Input.mousePosition;
        List<RaycastResult> results = new List<RaycastResult>();
        foreach(RaycastResult result in results)
        {
            if (result.gameObject.GetComponentInParent<Slider>())
            {
                
                onSlider = true;
                int n = result.gameObject.transform.parent.GetSiblingIndex();
                Debug.Log(n);
                descPanel.text = (knownParameters[n]) ? texts[n] : lockedDesc;
            }
        }
        if (!onSlider) descPanel.text = defaultDesc;
    }
    private void OnDisable()
    {
        Time.timeScale = 1;
    }
    private void OnEnable()
    {
        Time.timeScale = 0;
    }
    public void CloseTab()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }
    public void OpenTab()
    {
        Time.timeScale = 0;
        gameObject.SetActive(true);
    }

    public void UnlockRandomDesc()
    {
        if (GameManager.instance.money < 100 || knownParameters.All(x=>x==true)) return;

        GameManager.instance.money -= 100;
        bool done = false;
        do
        {
            int n = Random.Range(0, 9);
            if (!knownParameters[n])
            {
                knownParameters[n] = true;
                sliders[n].GetComponentInChildren<Text>().text = texts[n];
                done = true;
            }
        } while (!done);
    }

    public void OpenUpdateMenu()
    {
        if (gameObject.activeSelf) CloseTab();
        Time.timeScale = 0;
        updateMenu.SetActive(true);
    }

    public void CloseUpdateMenu()
    {
        Time.timeScale = 1;
        updateMenu.SetActive(false);
    }

    public void ApplyChanges()
    {
        Debug.Log("Applying changes to virus...");

        fert = sliders[0].GetComponentInChildren<Slider>().value;
        lifeT = sliders[1].GetComponentInChildren<Slider>().value;
        pR = sliders[2].GetComponentInChildren<Slider>().value;
        pG = sliders[3].GetComponentInChildren<Slider>().value;
        pB = sliders[4].GetComponentInChildren<Slider>().value;
        fR = sliders[5].GetComponentInChildren<Slider>().value;
        fG = sliders[6].GetComponentInChildren<Slider>().value;
        fB = sliders[7].GetComponentInChildren<Slider>().value;
        stab = sliders[8].GetComponentInChildren<Slider>().value;

        //if (current==null) current = def;
        Debug.Log(JsonUtility.ToJson(current));
        for(int i = 0; i < 12; i++)
        {
            //if (PlantManager.staticRef.plants[i / 4][i % 4]) PlantManager.staticRef.plants[i / 4][i % 4].dna = current;
        }
        GameManager.instance.airSound.PlayDelayed(.5f);
        Debug.Log("Virus is applied");
        CloseTab();
    }
}
