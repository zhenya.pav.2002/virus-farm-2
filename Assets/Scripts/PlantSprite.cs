using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "PlantSprite", menuName = "GameData/PlantSprite")]
public class PlantSprite : ScriptableObject
{
    /// <summary>
    /// Textures through it will loop
    /// </summary>
    public Sprite[] textures;
    public Sprite fruit;
}