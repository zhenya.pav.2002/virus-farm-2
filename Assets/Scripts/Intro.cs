﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Intro : MonoBehaviour
{
	[SerializeField]
	private float duration = 3f;
	[SerializeField]
	private Image image;
	private void Start()
	{
		StartCoroutine(IntroCutscene(duration));
	}
	private IEnumerator IntroCutscene(float duration)
	{
		Color clear = new Color(1, 1, 1, 0);
		image.color = clear;
		yield return image.FadeTo(Color.white, duration / 4);
		yield return new WaitForSecondsRealtime(duration / 2);
		yield return image.FadeTo(clear, duration / 4);

		PlayerPrefs.SetInt(GameManager.shownIntroSceneKey, 1);
		SceneManager.LoadSceneAsync(GameManager.GameSceneName);
	}
}
