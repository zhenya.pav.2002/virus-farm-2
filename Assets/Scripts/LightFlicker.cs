﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class LightFlicker : MonoBehaviour
{
	[SerializeField]
	private float range, interval = .1f;

	private float startingIntensity;
    private new Light2D light;
	private void Awake()
	{
		light = GetComponent<Light2D>();
		startingIntensity = light.intensity;
		StartCoroutine(Flicker());
	}
	private IEnumerator Flicker()
    {
		while (true)
		{
			yield return new WaitForSecondsRealtime(interval);
			light.intensity = startingIntensity + Random.Range(-range / 2, range / 2);
		}
		
    }
}
