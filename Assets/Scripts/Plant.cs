using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class Plant : MonoBehaviour
{
    public PlantDNA dna;
    public float lifeTime = 0;
    public bool hasFruit = false;
    [Header("Animation")]
    public float animDeltaTime = .5f;
    public PlantSprite currentSprite, adultSprite;

    [SerializeField]
    private Image timer;

    [SerializeField]
    private bool animationOnly = false;
    SpriteRenderer sprite;
    SpriteRenderer fruit;

    [System.Serializable]
    public class PlantSaveData
    {
        public string dna;
        public float lifeTime;
        public bool hasFruit;
    }
    public PlantSaveData saveData;
    public void Deserialize(string data)
    {
        saveData = JsonUtility.FromJson(data, typeof(PlantSaveData)) as PlantSaveData;
        dna = ScriptableObject.CreateInstance<PlantDNA>();
        JsonUtility.FromJsonOverwrite(saveData.dna, dna);
        lifeTime = saveData.lifeTime;
        hasFruit = saveData.hasFruit;

        fruit.gameObject.SetActive(false);
    }
    public string Serialize()
    {
        saveData.dna = JsonUtility.ToJson(dna);
        saveData.lifeTime = lifeTime;
        saveData.hasFruit = hasFruit;
        return JsonUtility.ToJson(saveData);
    }
    public void Initialize()
	{
        sprite.color = dna.color;
        fruit.color = dna.fruitColor;
        adultSprite = dna.form;
        fruit.sprite = adultSprite.fruit;
    }
    public void Awake()
    {
        sprite = GetComponent<SpriteRenderer>();
        fruit = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        if (!animationOnly)
        {
            currentSprite = PlantManager.staticRef.sprout;
            fruit.gameObject.SetActive(false);
            Initialize();
        }
        if(timer) timer.enabled = !animationOnly;
        StartCoroutine(Animate());
    }

    public IEnumerator Animate()
    {
        yield return new WaitForSeconds(animDeltaTime * Random.value * 2);
        int i = 0;
        while (true)
        {
            if (!animationOnly && lifeTime > PlantDNA.adultAge) currentSprite = adultSprite;
            if (!animationOnly && lifeTime > PlantDNA.fruitAge) Fruit();
            yield return new WaitForSeconds(animDeltaTime);
            sprite.sprite = currentSprite.textures[i];
            i++;
            if (i >= currentSprite.textures.Length) i = 0;
        }
    }
    public void Fruit()
    {
        Debug.Log("Fruit", this);
        hasFruit = true;
        fruit.gameObject.SetActive(true);
        Invoke(nameof(CollectFruit), 10f);
    }
	private void FixedUpdate()
	{
        lifeTime += Time.fixedDeltaTime;
        if(timer) timer.fillAmount = lifeTime / PlantDNA.fruitAge;
    }
	public void CollectFruit()
    {
        Debug.Log("Collect Fruit", this);
        hasFruit = false;
        GameManager.instance.money += dna.fruitPrice;
        GameManager.instance.moneySound.Play();
        fruit.gameObject.SetActive(false);
        Die();
    }
    public void Die()
    {
        Debug.Log("Death", this);
        Destroy(gameObject);
    }
}


