using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController staticRef;
    Rigidbody2D rb;
    Animator animator;
    Vector3 scaleInv = new Vector3(-1, 1, 1);
    public float moveSpeed = 1;
    [Header("Interaction")]
    public Transform apparatus, button;
    public float zoneSize;
    public GameObject interactionText, buttonText;
    public float xInput; 
    // Start is called before the first frame update
    void Awake()
    {
        staticRef = this;
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

	private void Start()
	{
		Minigames.MinigameManager.Instance.OnAllMinigamesComplete += ReEnablePlayer;

    }
	private void OnDestroy()
	{
		Minigames.MinigameManager.Instance.OnAllMinigamesComplete -= ReEnablePlayer;

    }
	private void ReEnablePlayer() => enabled = true;

	void FixedUpdate()
    {
#if !UNITY_ANDROID
        xInput = Input.GetAxis("Horizontal");
#endif

        if (xInput != 0)
        {
            rb.MovePosition((Vector2)transform.position + (new Vector2(xInput, 0) * moveSpeed * Time.fixedDeltaTime));
            animator.SetBool("walking", true);
            transform.localScale = (xInput < 0) ? scaleInv : Vector3.one;
        }else
        {
            animator.SetBool("walking", false);
        }
        
    }
    private void Update()
    {
        if (Mathf.Abs(transform.position.x - apparatus.position.x) < zoneSize/2)
        {
            interactionText.SetActive(true);
            if (Input.GetButtonDown("Interact"))
            {
                Debug.Log("Interact");
                interactionText.SetActive(false);
				Minigames.MinigameManager.Instance.StartMinigames();
                enabled = false;
            }
        } else interactionText.SetActive(false);

        /*if (Mathf.Abs(transform.position.x - button.position.x) < 1)
        {
            buttonText.SetActive(true);
            if (Input.GetButtonDown("Interact"))
            {
                FruitCollectionScript.staticRef.GetComponent<Animator>().SetTrigger("Press");
                FruitCollectionScript.staticRef.CollectFruits();
            }
        }
        else buttonText.SetActive(false);
        */

        //if (Input.GetButtonDown("UpgradeMenu")) MutationController.staticRef.OpenUpdateMenu();
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(apparatus.position,new Vector3(zoneSize,5));
    }
}
